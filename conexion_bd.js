const oracledb = require('oracledb');
    oracledb.createPool({
        user    :"usuario",
        password:"contraseña",
        connectString:"localhost",
        poolMax: 20,
        poolMin: 20,
        poolIncrement: 0
    },
    function(err, pool){
        if (err) {console.error(err.message); return;}
            pool.getConnection(function(err, connection){
                if (err) {console.error(err.message); return;}

                connection.close(
                    function(err){
                        if (err) {console.error(err.message);}
                    }
                );
            });
    }
);