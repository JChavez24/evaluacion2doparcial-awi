function guardarDatos(){
    const conexion = require('./conexion_bd')

    module.exports = app => {
    const connect = conexion

    app.post('/registro_datos', (req, res) => {
        const cedula = req.body.cedula
        const apellidos = req.body.apellidos
        const nombres = req.body.nombres
        const direccion = req.body.direccion
        const semestre = req.body.semestre
        const paralelo = req.body.paralelo
        const correoelec = req.body.correoelec

        if (cedula=="" || apellidos=="" || nombres==""  || direccion=="" || semestre=="" || paralelo=="" || correoelec=="") {          
            return res.render('createCli', {
                message: 'Faltan datos por ingresar!'
            });
        }

        connect.query('INSERT INTO estudiantes SET ?', {
            Cedula: cedula, Apellidos: apellidos, Nombres: nombres, 
            Direccion: direccion, Semestre: semestre, Paralelo: paralelo, Correo: correoelec
            }, (error, res) => {
                if (error){
                    console.log(error);
                }
                else{
                    res.redirect('/registro');
                    alert('Datos guardados con éxito!');
                }
            })
    })
    }
}

